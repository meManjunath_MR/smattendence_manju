﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginFrm.aspx.cs" Inherits="AttendenceSystemWeb.LoginFrm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>SM Attendence - log in</title>
    <link rel="stylesheet" href="Styles/style.css"/>
    <link rel="stylesheet prefetch" href="Styles/Bootstrap.min.css"/>
</head>
<body>
    
    <div class="wrapper" runat="server">
    <form class="form-signin" runat="server">       
      <h3 class="form-signin-heading"><asp:Label runat="server" ID="statuslbl" Text="Label"></asp:Label></h3>
      <input type="text" class="form-control" name="txtName" id="txtName" placeholder="Username" required="" autofocus="" value="<% =txtUserNameValue %>"  />
      <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="Password" required="" value="<% =txtPasswordValue %>" />      
     <button class="btn btn-lg btn-primary btn-block" type="submit" runat="server" onServerClick="btnLogin_Click">Login</button> 
     <button class="btn btn-lg btn-primary btn-block" type="submit" runat="server" onclick="return validate();">Clear</button>   
    </form>
  </div>

   <script type="text/javascript">
       function validate() {
           document.getElementById("txtName").value = "";
           document.getElementById("txtPassword").value = "";
           document.getElementById("txtName").focus();
       }
   </script>
</body>
</html>
