﻿using MySql.Data.MySqlClient;
using System;
using System.Configuration;
using System.Data;

namespace AttendenceSystemWeb
{
    public partial class LoginFrm : System.Web.UI.Page
    {

        private string usrName;
        private string usrPassword;
        public string txtUserNameValue;
        public string txtPasswordValue;

        string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.RemoveAll();
            statuslbl.Text = "Sign - In";
        }

        public void clearData()
        {
            txtUserNameValue = String.Empty;
            txtPasswordValue = String.Empty;
        }
        
        protected void btnLogin_Click(object sender, EventArgs e)
        {

            usrName = Request.Form["txtName"];
            usrPassword = Request.Form["txtPassword"];

            if (string.IsNullOrEmpty(usrName) || string.IsNullOrEmpty(usrPassword))
            {
                statuslbl.Text = "Please Enter Username and Password to login";
                clearData();
            }
            else
            {
                try
                {
                    using (MySqlConnection Conn = new MySqlConnection(ConnectionString))
                    {
                        Conn.Open();
                        string Query = "SELECT * FROM userdetails WHERE username='" + usrName + "' AND password= '" + usrPassword + "'";
                        using (MySqlCommand cmd = new MySqlCommand(Query, Conn))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@UserName", usrName);
                            cmd.Parameters.AddWithValue("@Password", usrPassword);
                            MySqlDataReader reader = cmd.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    Session["userID"] = reader["id"];
                                }

                                Response.Redirect("Dashboard.aspx", false);
                            }
                            else
                            {
                                statuslbl.Text = "Invalid Username/Password";
                                clearData();
                            }
                            Conn.Close();

                        }
                    }

                }
                catch (Exception ex)
                {
                    statuslbl.Text = ex.Message;
                }
            }
        }
    }
}