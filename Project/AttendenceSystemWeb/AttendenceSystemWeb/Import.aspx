﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainFrm.Master" AutoEventWireup="true" CodeBehind="Import.aspx.cs" Inherits="AttendenceSystemWeb.Import" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Import Employee</h2>
    <asp:FileUpload ID="FileUpload" runat="server" /><br />
    <asp:Button ID="uploadbtn" runat="server" Text="Upload" OnClick="uploadbtn_Click" /><br />
    <asp:Label ID="statuslbl" runat="server"></asp:Label><br />
    <asp:GridView ID="EmpGridView" runat="server"></asp:GridView>
</asp:Content>
