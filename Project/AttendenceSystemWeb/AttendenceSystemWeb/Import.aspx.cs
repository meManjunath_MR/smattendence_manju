﻿using System;
using System.Data.Common;
using System.Data.OleDb;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using System.IO;
using System.Collections.Generic;
using System.Web.UI;

namespace AttendenceSystemWeb
{
    public partial class Import : System.Web.UI.Page
    {
        //Connection String 
        string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
        DataTable empdt = null;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void uploadbtn_Click(object sender, EventArgs e)
        {
            string conStr, path;
            List<string> sheet = new List<string>();
            int count = 0;

            string Excel07ConString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0'";//Excel 2007 and higher
            if (FileUpload.HasFile)
            {
                try
                {
                    string filename = Path.GetFileName(FileUpload.FileName);
                    FileUpload.SaveAs(Server.MapPath("~/") + filename);

                    path = Server.MapPath("~/") + filename;
                    conStr = string.Format(Excel07ConString, path);

                    //Get the name of the First Sheet.
                    using (OleDbConnection con = new OleDbConnection(conStr))
                    {
                        using (OleDbCommand cmd = new OleDbCommand())
                        {
                            cmd.Connection = con;
                            con.Open();
                            DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            //sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            for(int i=0;i<dtExcelSchema.Rows.Count;i++)
                            {
                                sheet.Add(dtExcelSchema.Rows[i]["TABLE_NAME"].ToString());
                            }

                            con.Close();
                        }
                    }

                    empdt = new DataTable();
                    foreach (string sheetdata in sheet)
                    {
                        //Read Data from the First Sheet.
                        using (OleDbConnection con = new OleDbConnection(conStr))
                        {
                            using (OleDbCommand cmd = new OleDbCommand())
                            {
                                using (OleDbDataAdapter oda = new OleDbDataAdapter())
                                {
                                    cmd.CommandText = "SELECT * From [" + sheetdata + "]";
                                    cmd.Connection = con;
                                    con.Open();
                                    oda.SelectCommand = cmd;


                                    oda.Fill(empdt);
                                    con.Close();
                                }
                            }
                        }
                    }

                    //insert into database
                    count = insertEmpData();
                    statuslbl.Text = "Total no of Employee Details Imported : " + count;

                    EmpGridView.DataSource = empdt;
                    EmpGridView.DataBind();
                    
                }
                catch (Exception ex)
                {
                    statuslbl.Text = ex.Message+ex.StackTrace;
                }
            }
        }

        private int insertEmpData()
        {
            string cmdString = string.Empty;
            int insertcount = 0;
            try
            {
                cmdString = "INSERT INTO `employee`(`name`, `empcode`, `designation`, `phoneno`, `altphoneno`, `bloodgrp`, `photo`, )"+
                            " VALUES (@name,@empcode,@designation,@phoneno ,@altphone,@bloodgrp,@photo)";
                foreach (DataRow row in empdt.Rows)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ClientScript", "alert('" + row["empcode"] + "')", true);
                    insertcount++;
                    using (MySqlConnection Conn = new MySqlConnection(ConnectionString))
                    {
                        Conn.Open();
                        using (MySqlCommand cmd = new MySqlCommand(cmdString, Conn))
                        {
                            cmd.CommandType = CommandType.Text;

                            if (row["name"] == DBNull.Value)
                            {
                                cmd.Parameters.AddWithValue("@name", "");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@name", row["name"].ToString());
                            }

                            cmd.Parameters.AddWithValue("@empcode", row["empcode"].ToString());

                            if (row["designation"] == DBNull.Value)
                            {
                                cmd.Parameters.AddWithValue("@designation", "");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@designation", row["designation"].ToString());
                            }

                            if (row["phoneno"] == DBNull.Value)
                            {
                                cmd.Parameters.AddWithValue("@phoneno", "0");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@phoneno", row["phoneno"].ToString());
                            }

                            if (row["altphoneno"] == DBNull.Value)
                            {
                                cmd.Parameters.AddWithValue("@altphoneno", "0");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@altphoneno", row["altphoneno"].ToString());
                            }

                            if (row["bloodgrp"] == DBNull.Value)
                            {
                                cmd.Parameters.AddWithValue("@bloodgrp", "");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@bloodgrp", row["bloodgrp"].ToString());
                            }

                            if (row["photo"] == DBNull.Value)
                            {
                                cmd.Parameters.AddWithValue("@photo", "0");
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@photo", row["photo"].ToString());
                            }
                            cmd.ExecuteNonQuery();
                            Conn.Close();
                        }
                    }
                }
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), "ClientScript", "alert('" + insertcount + "')", true);
            }
            catch(Exception ex)
            {
                statuslbl.Text = ex.Message;
            }
            return insertcount;
        }
    }
}