﻿namespace RFIDReaderClassLib
{
    public class Antenna
    {
        #region VariableDeclaration

        private int id;
        private string altname;
        private string name;
        private int power;

        #endregion

        #region Constructor
        public Antenna()
        {
            id = 0;
            altname = string.Empty;
            name = string.Empty;
            power = 0;
        }

        public Antenna(int id,string altname,string name,int power)
        {
            this.id = id;
            this.altname = altname;
            this.name = name;
            this.power = power;
        }
        #endregion

        #region GetterSetter

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        public int Power
        {
            get { return power; }
            set { power = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string AltName
        {
            get { return altname; }
            set { altname = value; }
        }

        #endregion
    }
}
