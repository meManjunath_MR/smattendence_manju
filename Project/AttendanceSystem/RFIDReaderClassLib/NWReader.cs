﻿using System;
using System.Collections.Generic;

namespace RFIDReaderClassLib
{
    public class NWReader 
    {
        #region VariableDeclaration

        private int id;
        private string model;
        private string altname;
        private string ipaddress;
        private Boolean beep;
        private List<Antenna> antennalist;

        #endregion

        #region Constructor
        public NWReader()
        {
            id = 0;
            model = string.Empty;
            altname = string.Empty;
            ipaddress = string.Empty;
            beep = false;
            antennalist = new List<Antenna>();
        }

        public NWReader(int id,string model,string altname,string ipaddress,Boolean beep,List<Antenna> antennalist)
        {
            this.id = id;
            this.model = model;
            this.altname = altname;
            this.ipaddress = ipaddress;
            this.beep = beep;
            this.antennalist = antennalist;
        }

        #endregion

        #region GetterSetter
        public int ReaderID
        {
            get { return id; }
            set { id = value; }
        }

        public string Model
        {
            get { return model; }
            set { model = value; }
        }

        public string ReaderAltName
        {
            get { return altname; }
            set { altname = value; }
        }

        public string IpAddress
        {
            get { return ipaddress; }
            set { ipaddress = value; }
        }

        public Boolean Beep
        {
            get { return beep; }
            set { beep = value; }
        }

        public List<Antenna> AntennaList
        {
            get { return antennalist; }
            set { antennalist = value; }
        }

        #endregion

    }
}
