﻿namespace AttendanceSystem
{
    partial class ReaderSettingFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ReaderListView = new System.Windows.Forms.ListView();
            this.HeadingLbl = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AntennaGroup = new System.Windows.Forms.GroupBox();
            this.AntennaPanel = new System.Windows.Forms.Panel();
            this.BeepGroup = new System.Windows.Forms.GroupBox();
            this.BeepOffRadio = new System.Windows.Forms.RadioButton();
            this.BeepOnRadio = new System.Windows.Forms.RadioButton();
            this.IpAddressTxt = new System.Windows.Forms.TextBox();
            this.IpAddressLbl = new System.Windows.Forms.Label();
            this.ReaderALtNameLbl = new System.Windows.Forms.Label();
            this.UpdateBtn = new System.Windows.Forms.Button();
            this.RefreshBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.AntennaGroup.SuspendLayout();
            this.BeepGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // ReaderListView
            // 
            this.ReaderListView.FullRowSelect = true;
            this.ReaderListView.GridLines = true;
            this.ReaderListView.HideSelection = false;
            this.ReaderListView.Location = new System.Drawing.Point(12, 43);
            this.ReaderListView.Name = "ReaderListView";
            this.ReaderListView.Size = new System.Drawing.Size(155, 406);
            this.ReaderListView.TabIndex = 0;
            this.ReaderListView.UseCompatibleStateImageBehavior = false;
            this.ReaderListView.SelectedIndexChanged += new System.EventHandler(this.ReaderListView_SelectedIndexChanged);
            // 
            // HeadingLbl
            // 
            this.HeadingLbl.AutoSize = true;
            this.HeadingLbl.Location = new System.Drawing.Point(275, 9);
            this.HeadingLbl.Name = "HeadingLbl";
            this.HeadingLbl.Size = new System.Drawing.Size(138, 19);
            this.HeadingLbl.TabIndex = 1;
            this.HeadingLbl.Text = "RFID Reader Setting";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CancelBtn);
            this.panel1.Controls.Add(this.RefreshBtn);
            this.panel1.Controls.Add(this.UpdateBtn);
            this.panel1.Controls.Add(this.AntennaGroup);
            this.panel1.Controls.Add(this.BeepGroup);
            this.panel1.Controls.Add(this.IpAddressTxt);
            this.panel1.Controls.Add(this.IpAddressLbl);
            this.panel1.Controls.Add(this.ReaderALtNameLbl);
            this.panel1.Location = new System.Drawing.Point(173, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(497, 404);
            this.panel1.TabIndex = 2;
            // 
            // AntennaGroup
            // 
            this.AntennaGroup.Controls.Add(this.AntennaPanel);
            this.AntennaGroup.Location = new System.Drawing.Point(13, 92);
            this.AntennaGroup.Name = "AntennaGroup";
            this.AntennaGroup.Size = new System.Drawing.Size(466, 168);
            this.AntennaGroup.TabIndex = 4;
            this.AntennaGroup.TabStop = false;
            this.AntennaGroup.Text = "Antenna";
            // 
            // AntennaPanel
            // 
            this.AntennaPanel.AutoScroll = true;
            this.AntennaPanel.Location = new System.Drawing.Point(6, 17);
            this.AntennaPanel.Name = "AntennaPanel";
            this.AntennaPanel.Size = new System.Drawing.Size(454, 145);
            this.AntennaPanel.TabIndex = 0;
            // 
            // BeepGroup
            // 
            this.BeepGroup.Controls.Add(this.BeepOffRadio);
            this.BeepGroup.Controls.Add(this.BeepOnRadio);
            this.BeepGroup.Location = new System.Drawing.Point(13, 266);
            this.BeepGroup.Name = "BeepGroup";
            this.BeepGroup.Size = new System.Drawing.Size(466, 74);
            this.BeepGroup.TabIndex = 3;
            this.BeepGroup.TabStop = false;
            this.BeepGroup.Text = "Beep";
            // 
            // BeepOffRadio
            // 
            this.BeepOffRadio.AutoSize = true;
            this.BeepOffRadio.Location = new System.Drawing.Point(310, 26);
            this.BeepOffRadio.Name = "BeepOffRadio";
            this.BeepOffRadio.Size = new System.Drawing.Size(52, 23);
            this.BeepOffRadio.TabIndex = 1;
            this.BeepOffRadio.TabStop = true;
            this.BeepOffRadio.Text = "OFF";
            this.BeepOffRadio.UseVisualStyleBackColor = true;
            // 
            // BeepOnRadio
            // 
            this.BeepOnRadio.AutoSize = true;
            this.BeepOnRadio.Location = new System.Drawing.Point(137, 26);
            this.BeepOnRadio.Name = "BeepOnRadio";
            this.BeepOnRadio.Size = new System.Drawing.Size(48, 23);
            this.BeepOnRadio.TabIndex = 0;
            this.BeepOnRadio.TabStop = true;
            this.BeepOnRadio.Text = "ON";
            this.BeepOnRadio.UseVisualStyleBackColor = true;
            // 
            // IpAddressTxt
            // 
            this.IpAddressTxt.Location = new System.Drawing.Point(277, 50);
            this.IpAddressTxt.Name = "IpAddressTxt";
            this.IpAddressTxt.Size = new System.Drawing.Size(150, 27);
            this.IpAddressTxt.TabIndex = 2;
            // 
            // IpAddressLbl
            // 
            this.IpAddressLbl.AutoSize = true;
            this.IpAddressLbl.Location = new System.Drawing.Point(77, 50);
            this.IpAddressLbl.Name = "IpAddressLbl";
            this.IpAddressLbl.Size = new System.Drawing.Size(77, 19);
            this.IpAddressLbl.TabIndex = 1;
            this.IpAddressLbl.Text = "IP Address";
            // 
            // ReaderALtNameLbl
            // 
            this.ReaderALtNameLbl.AutoSize = true;
            this.ReaderALtNameLbl.Location = new System.Drawing.Point(177, 17);
            this.ReaderALtNameLbl.Name = "ReaderALtNameLbl";
            this.ReaderALtNameLbl.Size = new System.Drawing.Size(97, 19);
            this.ReaderALtNameLbl.TabIndex = 0;
            this.ReaderALtNameLbl.Text = "Reader Name";
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.Location = new System.Drawing.Point(39, 357);
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.Size = new System.Drawing.Size(85, 31);
            this.UpdateBtn.TabIndex = 5;
            this.UpdateBtn.Text = "Update";
            this.UpdateBtn.UseVisualStyleBackColor = true;
            this.UpdateBtn.Click += new System.EventHandler(this.UpdateBtn_Click);
            // 
            // RefreshBtn
            // 
            this.RefreshBtn.Location = new System.Drawing.Point(208, 357);
            this.RefreshBtn.Name = "RefreshBtn";
            this.RefreshBtn.Size = new System.Drawing.Size(83, 31);
            this.RefreshBtn.TabIndex = 6;
            this.RefreshBtn.Text = "Refresh";
            this.RefreshBtn.UseVisualStyleBackColor = true;
            this.RefreshBtn.Click += new System.EventHandler(this.RefreshBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.Location = new System.Drawing.Point(363, 357);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(88, 31);
            this.CancelBtn.TabIndex = 7;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = true;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // ReaderSettingFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 461);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeadingLbl);
            this.Controls.Add(this.ReaderListView);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ReaderSettingFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reader Setting";
            this.Load += new System.EventHandler(this.ReaderSettingFrm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.AntennaGroup.ResumeLayout(false);
            this.BeepGroup.ResumeLayout(false);
            this.BeepGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView ReaderListView;
        private System.Windows.Forms.Label HeadingLbl;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox BeepGroup;
        private System.Windows.Forms.RadioButton BeepOffRadio;
        private System.Windows.Forms.RadioButton BeepOnRadio;
        private System.Windows.Forms.TextBox IpAddressTxt;
        private System.Windows.Forms.Label IpAddressLbl;
        private System.Windows.Forms.Label ReaderALtNameLbl;
        private System.Windows.Forms.GroupBox AntennaGroup;
        private System.Windows.Forms.Panel AntennaPanel;
        private System.Windows.Forms.Button RefreshBtn;
        private System.Windows.Forms.Button UpdateBtn;
        private System.Windows.Forms.Button CancelBtn;
    }
}