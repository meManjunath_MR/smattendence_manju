﻿namespace AttendanceSystem
{
    partial class AddReaderFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeadingLbl = new System.Windows.Forms.Label();
            this.Info1Lbl = new System.Windows.Forms.Label();
            this.ReaderGroup = new System.Windows.Forms.GroupBox();
            this.ReaderStatusLbl = new System.Windows.Forms.Label();
            this.CheckBtn = new System.Windows.Forms.Button();
            this.IpTxt = new System.Windows.Forms.TextBox();
            this.IpLbl = new System.Windows.Forms.Label();
            this.ReaderPanel = new System.Windows.Forms.Panel();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.AltNameTxt = new System.Windows.Forms.TextBox();
            this.AltNameLbl = new System.Windows.Forms.Label();
            this.AntennaGroup = new System.Windows.Forms.GroupBox();
            this.AntennaPanel = new System.Windows.Forms.Panel();
            this.SaveAntBtn = new System.Windows.Forms.Button();
            this.AddAntBtn = new System.Windows.Forms.Button();
            this.ReaderGroup.SuspendLayout();
            this.ReaderPanel.SuspendLayout();
            this.AntennaGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeadingLbl
            // 
            this.HeadingLbl.AutoSize = true;
            this.HeadingLbl.Location = new System.Drawing.Point(223, 23);
            this.HeadingLbl.Name = "HeadingLbl";
            this.HeadingLbl.Size = new System.Drawing.Size(118, 19);
            this.HeadingLbl.TabIndex = 0;
            this.HeadingLbl.Text = "Add RFID Reader";
            // 
            // Info1Lbl
            // 
            this.Info1Lbl.AutoSize = true;
            this.Info1Lbl.Location = new System.Drawing.Point(40, 65);
            this.Info1Lbl.Name = "Info1Lbl";
            this.Info1Lbl.Size = new System.Drawing.Size(338, 19);
            this.Info1Lbl.TabIndex = 1;
            this.Info1Lbl.Text = "Please Provide the Details of Network RFID Reader";
            // 
            // ReaderGroup
            // 
            this.ReaderGroup.Controls.Add(this.ReaderStatusLbl);
            this.ReaderGroup.Controls.Add(this.CheckBtn);
            this.ReaderGroup.Controls.Add(this.IpTxt);
            this.ReaderGroup.Controls.Add(this.IpLbl);
            this.ReaderGroup.Location = new System.Drawing.Point(44, 91);
            this.ReaderGroup.Name = "ReaderGroup";
            this.ReaderGroup.Size = new System.Drawing.Size(487, 159);
            this.ReaderGroup.TabIndex = 2;
            this.ReaderGroup.TabStop = false;
            this.ReaderGroup.Text = "Reader Details";
            // 
            // ReaderStatusLbl
            // 
            this.ReaderStatusLbl.Location = new System.Drawing.Point(6, 107);
            this.ReaderStatusLbl.Name = "ReaderStatusLbl";
            this.ReaderStatusLbl.Size = new System.Drawing.Size(475, 27);
            this.ReaderStatusLbl.TabIndex = 3;
            this.ReaderStatusLbl.Text = "Reader Status";
            this.ReaderStatusLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CheckBtn
            // 
            this.CheckBtn.Location = new System.Drawing.Point(180, 56);
            this.CheckBtn.Name = "CheckBtn";
            this.CheckBtn.Size = new System.Drawing.Size(115, 35);
            this.CheckBtn.TabIndex = 2;
            this.CheckBtn.Text = "Check Reader";
            this.CheckBtn.UseVisualStyleBackColor = true;
            this.CheckBtn.Click += new System.EventHandler(this.CheckBtn_Click);
            // 
            // IpTxt
            // 
            this.IpTxt.Location = new System.Drawing.Point(248, 23);
            this.IpTxt.Name = "IpTxt";
            this.IpTxt.Size = new System.Drawing.Size(169, 27);
            this.IpTxt.TabIndex = 1;
            // 
            // IpLbl
            // 
            this.IpLbl.AutoSize = true;
            this.IpLbl.Location = new System.Drawing.Point(65, 23);
            this.IpLbl.Name = "IpLbl";
            this.IpLbl.Size = new System.Drawing.Size(117, 19);
            this.IpLbl.TabIndex = 0;
            this.IpLbl.Text = "Static IP Address";
            // 
            // ReaderPanel
            // 
            this.ReaderPanel.Controls.Add(this.SaveBtn);
            this.ReaderPanel.Controls.Add(this.AltNameTxt);
            this.ReaderPanel.Controls.Add(this.AltNameLbl);
            this.ReaderPanel.Location = new System.Drawing.Point(44, 255);
            this.ReaderPanel.Name = "ReaderPanel";
            this.ReaderPanel.Size = new System.Drawing.Size(487, 92);
            this.ReaderPanel.TabIndex = 3;
            // 
            // SaveBtn
            // 
            this.SaveBtn.Location = new System.Drawing.Point(180, 45);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(115, 35);
            this.SaveBtn.TabIndex = 2;
            this.SaveBtn.Text = "Save Details";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // AltNameTxt
            // 
            this.AltNameTxt.Location = new System.Drawing.Point(248, 12);
            this.AltNameTxt.Name = "AltNameTxt";
            this.AltNameTxt.Size = new System.Drawing.Size(169, 27);
            this.AltNameTxt.TabIndex = 1;
            // 
            // AltNameLbl
            // 
            this.AltNameLbl.AutoSize = true;
            this.AltNameLbl.Location = new System.Drawing.Point(67, 12);
            this.AltNameLbl.Name = "AltNameLbl";
            this.AltNameLbl.Size = new System.Drawing.Size(115, 19);
            this.AltNameLbl.TabIndex = 0;
            this.AltNameLbl.Text = "Alternate Name ";
            // 
            // AntennaGroup
            // 
            this.AntennaGroup.Controls.Add(this.AddAntBtn);
            this.AntennaGroup.Controls.Add(this.SaveAntBtn);
            this.AntennaGroup.Controls.Add(this.AntennaPanel);
            this.AntennaGroup.Location = new System.Drawing.Point(44, 353);
            this.AntennaGroup.Name = "AntennaGroup";
            this.AntennaGroup.Size = new System.Drawing.Size(487, 204);
            this.AntennaGroup.TabIndex = 4;
            this.AntennaGroup.TabStop = false;
            this.AntennaGroup.Text = "Antenna";
            // 
            // AntennaPanel
            // 
            this.AntennaPanel.AutoScroll = true;
            this.AntennaPanel.Location = new System.Drawing.Point(180, 26);
            this.AntennaPanel.Name = "AntennaPanel";
            this.AntennaPanel.Size = new System.Drawing.Size(298, 127);
            this.AntennaPanel.TabIndex = 0;
            // 
            // SaveAntBtn
            // 
            this.SaveAntBtn.Location = new System.Drawing.Point(183, 159);
            this.SaveAntBtn.Name = "SaveAntBtn";
            this.SaveAntBtn.Size = new System.Drawing.Size(115, 35);
            this.SaveAntBtn.TabIndex = 1;
            this.SaveAntBtn.Text = "Save Antenna";
            this.SaveAntBtn.UseVisualStyleBackColor = true;
            // 
            // AddAntBtn
            // 
            this.AddAntBtn.Location = new System.Drawing.Point(40, 26);
            this.AddAntBtn.Name = "AddAntBtn";
            this.AddAntBtn.Size = new System.Drawing.Size(115, 35);
            this.AddAntBtn.TabIndex = 2;
            this.AddAntBtn.Text = "Add Antenna";
            this.AddAntBtn.UseVisualStyleBackColor = true;
            // 
            // AddReaderFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.AntennaGroup);
            this.Controls.Add(this.ReaderPanel);
            this.Controls.Add(this.ReaderGroup);
            this.Controls.Add(this.Info1Lbl);
            this.Controls.Add(this.HeadingLbl);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "AddReaderFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add RFID Reader";
            this.Load += new System.EventHandler(this.AddReaderFrm_Load);
            this.ReaderGroup.ResumeLayout(false);
            this.ReaderGroup.PerformLayout();
            this.ReaderPanel.ResumeLayout(false);
            this.ReaderPanel.PerformLayout();
            this.AntennaGroup.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeadingLbl;
        private System.Windows.Forms.Label Info1Lbl;
        private System.Windows.Forms.GroupBox ReaderGroup;
        private System.Windows.Forms.Label ReaderStatusLbl;
        private System.Windows.Forms.Button CheckBtn;
        private System.Windows.Forms.TextBox IpTxt;
        private System.Windows.Forms.Label IpLbl;
        private System.Windows.Forms.Panel ReaderPanel;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.TextBox AltNameTxt;
        private System.Windows.Forms.Label AltNameLbl;
        private System.Windows.Forms.GroupBox AntennaGroup;
        private System.Windows.Forms.Panel AntennaPanel;
        private System.Windows.Forms.Button SaveAntBtn;
        private System.Windows.Forms.Button AddAntBtn;
    }
}