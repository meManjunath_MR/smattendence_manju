﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceSystem
{
    public partial class LoginFrm : Form
    {
        #region VariableDeclartion
        //Variable Declaration
        RFIDReaderDB DbObj = new RFIDReaderDB();

        #endregion
        public LoginFrm()
        {
            InitializeComponent();
        }

        #region ExitBtn

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #region LoginBtn
        private void LoginBtn_Click(object sender, EventArgs e)
        {
            if(UserTxt.Text==string.Empty)
            {
                MessageBox.Show(this, "Username should not empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                UserTxt.Focus();
            }
            else if(Passtxt.Text==string.Empty)
            {
                MessageBox.Show(this, "Username should not empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                Passtxt.Focus();
            }
            else
            {
                if(DbObj.IsUserExist(UserTxt.Text.Trim(),Passtxt.Text))
                {
                    //Correct Login
                    MainFrm frm = new MainFrm();
                    frm.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show(this, "Invalid Username and Password", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    UserTxt.Focus();
                }
            }
        }
        #endregion

        #region ClearBtn
        private void ClearBtn_Click(object sender, EventArgs e)
        {
            ClearData();
        }
        #endregion

        #region ClearData
        /*
         * Functionality    :   Reset the Variable
         * Parameter        :   void
         * Return Type      :   void
        */
        private void ClearData()
        {
            UserTxt.Text = string.Empty;
            Passtxt.Text = string.Empty;
            UserTxt.Focus();
        }
        #endregion

    }
}
