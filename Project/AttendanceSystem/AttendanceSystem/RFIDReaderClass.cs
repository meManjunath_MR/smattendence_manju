﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;
using ThingMagic;

namespace AttendanceSystem
{
    class RFIDReaderClass
    {

        #region DeviceConnected 

        /*
         * Functionality    :   Checks whether gievn ipaddress of reader is connected or not
         * Parameter        :   host => static ipaddress of the Reader
         * Return Type      :   Boolean => returns true , if device is connected else false
        */

        public Boolean deviceConnected(string host)
        {
            //Local Variable Declaration
            Boolean flag = false;
            var ping = new Ping();
            var options = new PingOptions { DontFragment = true };
            var buffer = Encoding.ASCII.GetBytes(new string('z', 10)); //just need some data. this sends 10 bytes.

            //Main Logic
            try
            {
                if (host == string.Empty)
                {
                    return flag;
                }
                else
                {
                    var reply = ping.Send(host, 60, buffer, options);
                    if (reply.Status == IPStatus.Success)   //on successful ping status , flag is set to true
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = false;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error in Device Connectted : "+e.Message);
            }

            return flag;
        }

        #endregion

        #region GetReaderInfo

        /*
         * Functionality    :   Gets the Reader Model and Serial Number 
         * Parameter        :   host => static ipaddress of the Reader
         * Return Type      :   List of Key Value Pair that contains reader information 
         */

        public List<KeyValuePair<string,string>> GetReaderInfo(string host)
        {
            //Local Variable Declaration
            List<KeyValuePair<string, string>> readerInfo = new List<KeyValuePair<string, string>>();
            string[] args = new string[3];
            args[0] = @"tmr://"+host.Trim()+"/";
            args[1] = @"--ant";
            args[2] = @"1,2";

            //Main Logic
            try
            {
                using (Reader r = Reader.Create(args[0]))
                {
                    r.Connect();
                    // Serial info
                    readerInfo.Add(new KeyValuePair<string, string>("serial",(string) r.ParamGet("/reader/version/serial")));
                    // Model info
                    readerInfo.Add(new KeyValuePair<string, string>("model", (string)r.ParamGet("/reader/version/model")));
                    r.Destroy();
                }
            }
            catch (ReaderException re)
            {
                throw new ReaderException("Error in GetReaderInfo : "+re.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("Error in GetReaderInfo : " + ex.Message);
            }

            return readerInfo;
        }

        #endregion
    }
}
