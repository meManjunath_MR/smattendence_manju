﻿using System.Windows.Forms;

namespace AttendanceSystem
{
    public partial class MainFrm : Form
    {
        public MainFrm()
        {
            InitializeComponent();
        }

        private void addReaderToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            AddReaderFrm frm = new AddReaderFrm();
            frm.MdiParent = this;
            frm.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            Application.Exit();
        }

        private void configureReaderToolStripMenuItem_Click(object sender, System.EventArgs e)
        {
            ReaderSettingFrm frm = new ReaderSettingFrm();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
