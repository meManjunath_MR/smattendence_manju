﻿using RFIDReaderClassLib;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace AttendanceSystem
{
    public partial class ReaderSettingFrm : Form
    {
        #region VariableDeclaration
        //Local Variable Declaration
        RFIDReaderDB DBObj = new RFIDReaderDB();
        List<KeyValuePair<int, string>> readerList = new List<KeyValuePair<int, string>>();
        List<NWReader> readerListDetails = new List<NWReader>();
        NWReader selectedreader = new NWReader();

        Label[] AntennaLabel = null;
        Label[] AntennaAltLabel = null;
        TextBox[] PowerTxt = null;

        Label AntFieldName = null;
        Label AntFieldAltName = null;
        Label FieldPower = null;
        #endregion

        public ReaderSettingFrm()
        {
            InitializeComponent();
        }

        #region FromLoad
        private void ReaderSettingFrm_Load(object sender, EventArgs e)
        {
            try
            {
                SetReaderList(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error in Reader Setting From Load : " + ex.Message+ex.StackTrace, "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region SetReaderList

        /*
         * Functionality    :   Set the list of Reader Name 
         * Parameter        :   Void
         * Return Type      :   Void
         */

        private void SetReaderList(int index)
        {
            //Local Variable Declaration
            ListViewItem itm = null;
            string[] arr = new string[1];

            try
            {
                //Main Logic
                readerListDetails = DBObj.GetAllReaderDetails();
                readerList = DBObj.GetAllReaderName();
                if(readerList.Count>0)
                {
                    ReaderListView.View = View.Details;
                    ReaderListView.Items.Clear();
                    ReaderListView.Columns.Clear();
                    ReaderListView.Columns.Add("Reader Name", 150);
                }
                foreach(KeyValuePair<int, string> name in readerList)
                {
                    arr[0] = name.Value;
                    itm = new ListViewItem(arr);
                    ReaderListView.Items.Add(itm);
                }
                ReaderListView.Items[index].Selected = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error in Reader Setting SetReaderList : " + ex.Message + ex.StackTrace, "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region ReaderListViewSelected
        private void ReaderListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            try
            {
                var selectedItems = ReaderListView.SelectedItems;
                if (selectedItems.Count > 0)
                {
                    // Display text of first item selected.
                    foreach (KeyValuePair<int, string> name in readerList)
                    {
                        if (selectedItems[0].Text == name.Value)
                        {
                            selectedreader = readerListDetails.Find(x => x.ReaderID == name.Key);
                            ReaderALtNameLbl.Text = selectedreader.ReaderAltName;
                            IpAddressTxt.Text = selectedreader.IpAddress;
                            if(selectedreader.Beep)
                            {
                                BeepOnRadio.Checked = true;
                            }
                            else
                            {
                                BeepOffRadio.Checked = true;
                            }
                            GenerateAntennaType(selectedreader.AntennaList);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(this, "Error in Reader Setting List selected : " + ex.Message+ex.StackTrace, "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region GenerateAntennaType

        /*
         * Functionality    :   Dynamic Antenna Creation
         * Parameter        :   Antenna List
         * Return Type      :   Void
         */
        private void GenerateAntennaType(List<Antenna> AntList)
        {
            AntennaLabel = new Label[AntList.Count];
            AntennaAltLabel = new Label[AntList.Count];
            PowerTxt = new TextBox[AntList.Count];

            int i = 0;

            AntennaPanel.Controls.Clear();

            if(AntList.Count > 0)
            {
                AntFieldName = new Label();
                AntFieldName.Name = "fieldlbl";
                AntFieldName.Text = "Antenna Name";
                AntFieldName.Location = new System.Drawing.Point(30, 20);
                AntFieldName.Size = new System.Drawing.Size(300, 25);
                AntFieldName.AutoSize = true;
                AntennaPanel.Controls.Add(AntFieldName);

                AntFieldAltName = new Label();
                AntFieldAltName.Name = "fieldaltlbl";
                AntFieldAltName.Text = "Antenna Alt Name";
                AntFieldAltName.Location = new System.Drawing.Point(150, 20);
                AntFieldAltName.Size = new System.Drawing.Size(300, 25);
                AntFieldAltName.AutoSize = true;
                AntennaPanel.Controls.Add(AntFieldAltName);

                FieldPower = new Label();
                FieldPower.Name = "fieldaltlbl";
                FieldPower.Text = "Antenna Power";
                FieldPower.Location = new System.Drawing.Point(280, 20);
                FieldPower.Size = new System.Drawing.Size(300, 25);
                FieldPower.AutoSize = true;
                AntennaPanel.Controls.Add(FieldPower);
            }
            foreach (Antenna ant in AntList)
            {
                AntennaLabel[i] = new Label();
                AntennaLabel[i].Name = "lbl" + ant.ID.ToString();
                AntennaLabel[i].Text = ant.Name;
                AntennaLabel[i].Location = new System.Drawing.Point(30, 50 + (i * 60));
                AntennaLabel[i].Size = new System.Drawing.Size(300, 25);
                AntennaLabel[i].AutoSize = true;
                AntennaPanel.Controls.Add(AntennaLabel[i]);

                AntennaAltLabel[i] = new Label();
                AntennaAltLabel[i].Name = "altlbl" + ant.ID.ToString();
                AntennaAltLabel[i].Text = ant.AltName;
                AntennaAltLabel[i].Location = new System.Drawing.Point(150, 50 + (i * 60));
                AntennaAltLabel[i].Size = new System.Drawing.Size(300, 25);
                AntennaAltLabel[i].AutoSize = true;
                AntennaPanel.Controls.Add(AntennaAltLabel[i]);

                PowerTxt[i] = new TextBox();
                PowerTxt[i].Name = "powertxt" + ant.ID.ToString();
                PowerTxt[i].Text = ant.Power.ToString();
                PowerTxt[i].Location = new System.Drawing.Point(280, 50 + (i * 60));
                PowerTxt[i].Size = new System.Drawing.Size(100, 25);
                PowerTxt[i].AutoSize = true;
                AntennaPanel.Controls.Add(PowerTxt[i]);

                i++;
            }
        }
        #endregion

        #region RefreshButton
        private void RefreshBtn_Click(object sender, EventArgs e)
        {
            SetReaderList(0);
        }
        #endregion

        #region UpdateButton
        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            Match match = Regex.Match(IpAddressTxt.Text.Trim(), @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
            string query = string.Empty;
            List<string> field = new List<string>();
            Boolean BeepFlag = false,resultflag=false;
            int result = 0, i = 0, power = 0 ;

            if (selectedreader.ReaderID>0)
            {
                if(selectedreader.IpAddress!=IpAddressTxt.Text)
                {
                    //Check Whether that ip is not exists
                    if(IpAddressTxt.Text ==string.Empty)
                    {
                        MessageBox.Show(this, "IP Address Cannot be Empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        IpAddressTxt.Focus();
                    }
                    else if(!match.Success)
                    {
                        MessageBox.Show(this, "Please Enter Valid IP Address", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        IpAddressTxt.Focus();
                    }
                    else if(DBObj.IsReaderExist(IpAddressTxt.Text.Trim(),""))
                    {
                        MessageBox.Show(this, "IP Address already Exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        IpAddressTxt.Focus();
                    }
                    else
                    {
                        field.Add("ipaddress = '"+IpAddressTxt.Text+"'");
                    }
                }
                
                if(BeepOffRadio.Checked)
                {
                    BeepFlag = false;
                }
                if(BeepOnRadio.Checked)
                {
                    BeepFlag = true;
                }
                if (BeepFlag != selectedreader.Beep)
                {
                    if (BeepFlag)
                    {
                        field.Add("beep = '1'");
                    }
                    else
                    {
                        field.Add("beep = '0'");
                    }
                }

                foreach(Antenna antdata in selectedreader.AntennaList)
                {
                    if (PowerTxt[i].Text == string.Empty)
                    {
                        MessageBox.Show(this, "Power Should not be empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        PowerTxt[i].Focus();
                    }
                    else
                    {
                        if (PowerTxt[i].Text != antdata.Power.ToString())
                        {
                            power = Int32.Parse(PowerTxt[i].Text);
                            if (power > 10 && power <= 30)
                            {
                                //update the antenna power value
                                query = "UPDATE `antenna` SET `power`='" + PowerTxt[i].Text + "' WHERE `id`=" + antdata.ID;
                                result = DBObj.UpdateData(query);
                                resultflag = true;
                            }
                            else
                            {
                                MessageBox.Show(this, "Power Should be between 10 and 30 db", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                IpAddressTxt.Focus();
                            }
                        }
                    }
                    i++;
                }

                if (field.Count > 0)
                {
                    query = "UPDATE `reader` SET ";
                    foreach(string value in field)
                    {
                        query += value + ",";
                    }
                    query = query.Substring(0, query.Length - 1);
                    query += " WHERE `id`='"+selectedreader.ReaderID+"'";
                    result = DBObj.UpdateData(query);
                    resultflag = true;
                }
                if (resultflag)
                {
                    MessageBox.Show(this, "Reader Details Updated Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SetReaderList(ReaderListView.SelectedItems[0].Index);
                }
            }
        }

        #endregion
        #region CancelButton
        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        #endregion
    }
}
