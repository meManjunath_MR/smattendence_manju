﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using RFIDReaderClassLib;
using System.Windows.Forms;

namespace AttendanceSystem
{
    class RFIDReaderDB
    {

        #region VariableDeclaration
        //Variable Declaration
        string CmdString = string.Empty;

        MySqlConnection databaseConnection = null;
        MySqlCommand cmd = null;
        MySqlDataReader datareader = null;
        #endregion

        #region GetDBConnection

        /*
         * Functionality    :   Connect the MysqlConnection 
         * Parameter        :   Void
         * Return Type      :   MysqlCoonection Object 
         */
        public MySqlConnection GetDBConnection()
        {
            try
            {
                string connectionString = string.Empty;

                if (databaseConnection == null)
                {
                    connectionString = "SERVER=localhost;UID=root;DATABASE=rfidreader;PASSWORD=root;PORT=3306;";
                    databaseConnection = new MySqlConnection(connectionString);
                }
                return databaseConnection;
            }
            catch (Exception e)
            {
                throw new Exception("Error in GetDBConnection :" + e.Message);
            }
        }

        #endregion

        #region CloseConnection
        /*
         * Functionality    :   DisConnect the MysqlConnection 
         * Parameter        :   Void
         * Return Type      :   Void 
         */
        public void CloseConnection()
        {
            try
            {
                if (databaseConnection.State != ConnectionState.Closed)
                {
                    databaseConnection.Close();
                }
            }
            catch(Exception e)
            {
                throw new Exception("Error in CloseConnection :" + e.Message);
            }
        }

        #endregion

        #region InsertData
        /*
         * Functionality    :   Insert the data to database according to the cmd string 
         * Parameter        :   string query => sql insert statement
         * Return Type      :   int id => id of the row inserted 
         */
        public int InsertData(string query)
        {
            //Local Variable Declaration
            int insertId = 0;

            //Main Logic
            try
            {
                databaseConnection = GetDBConnection();
                CmdString = query;
                cmd = new MySqlCommand(CmdString, databaseConnection);
            
                if (databaseConnection.State != System.Data.ConnectionState.Open)
                {
                    databaseConnection.Open();
                }
                cmd.ExecuteNonQuery();
                insertId = (int)cmd.LastInsertedId;

                return insertId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in InsertData :" + ex.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion

        #region UpdateData
        /*
         * Functionality    :   Update Row according to reader
         * Parameter        :   string query => sql update statement
         * Return Type      :   int id => no of row inserted 
         */
        public int UpdateData(string query)
        {
            //Local Variable Declaration
            int insertId = 0;

            //Main Logic
            try
            {
                databaseConnection = GetDBConnection();
                CmdString = query;
                cmd = new MySqlCommand(CmdString, databaseConnection);

                if (databaseConnection.State != System.Data.ConnectionState.Open)
                {
                    databaseConnection.Open();
                }
                cmd.ExecuteNonQuery();
                insertId = (int)cmd.LastInsertedId;

                return insertId;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in UpdateData :" + ex.Message);
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion

        #region IsReaderExist

        /*
         * Functionality    :   Check Whether Reader details is already exist in Database 
         * Parameter        :   string ipaddress,string serialno,string altname
         * Return Type      :   Boolean => if reader details exist , then true else false
         */
        public bool IsReaderExist(string ipaddress,string altname)
        {
            bool flag = false;
            int count = 0;
            try
            {
                databaseConnection = GetDBConnection();
                if (databaseConnection == null)
                {
                    throw new Exception("DatabaseConnection is null ");
                }
                else
                {
                    if (databaseConnection.State != ConnectionState.Open)
                    {
                        databaseConnection.Open();
                    }
                    CmdString = "SELECT count(*) FROM `reader` WHERE ipaddress=@ipaddress or altname=@altname";
                    cmd = new MySqlCommand(CmdString, databaseConnection);
                    cmd.Parameters.AddWithValue("@ipaddress", ipaddress);
                    cmd.Parameters.AddWithValue("@altname", altname);
                    datareader = cmd.ExecuteReader();
                    while (datareader.Read())
                    {
                        count = Int32.Parse(datareader["count(*)"].ToString());
                        if (count > 0)
                        {
                            flag = true;
                            break;
                        }
                    }

                    datareader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return flag;
        }

        #endregion

        #region IsUserExist

        /*
         * Functionality    :   Check Whether User details is already exist in Database with right credentails
         * Parameter        :   string name , string password
         * Return Type      :   Boolean => if user details exist with right credentails , then true else false
         */
        public bool IsUserExist(string name, string password)
        {
            bool flag = false;
            int count = 0;
            try
            {
                databaseConnection = GetDBConnection();
                if (databaseConnection == null)
                {
                    throw new Exception("DatabaseConnection is null ");
                }
                else
                {
                    if (databaseConnection.State != ConnectionState.Open)
                    {
                        databaseConnection.Open();
                    }
                    CmdString = "SELECT count(*) FROM `userdetails` WHERE username=@username and password=@password ";
                    cmd = new MySqlCommand(CmdString, databaseConnection);
                    cmd.Parameters.AddWithValue("@username", name);
                    cmd.Parameters.AddWithValue("@password", password);
                    datareader = cmd.ExecuteReader();
                    while (datareader.Read())
                    {
                        count = Int32.Parse(datareader["count(*)"].ToString());
                        if (count > 0)
                        {
                            flag = true;
                            break;
                        }
                    }

                    datareader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return flag;
        }

        #endregion

        #region GetAllReaderName

        /*
         * Functionality    :   Get the list of Reader Name 
         * Parameter        :   Void
         * Return Type      :   List => list of Reader Name
         */
        public List<KeyValuePair<int,string>> GetAllReaderName()
        {
            //Local Variable Declaration
            List<KeyValuePair<int, string>> readerList = new List<KeyValuePair<int, string>>();

            //Main Logic
            try
            {
                databaseConnection = GetDBConnection();
                if (databaseConnection == null)
                {
                    throw new Exception("DatabaseConnection is null ");
                }
                else
                {
                    if (databaseConnection.State != ConnectionState.Open)
                    {
                        databaseConnection.Open();
                    }
                    CmdString = "SELECT id,altname FROM `reader` WHERE 1";
                    cmd = new MySqlCommand(CmdString, databaseConnection);
                    datareader = cmd.ExecuteReader();
                    while (datareader.Read())
                    {
                        readerList.Add(new KeyValuePair<int, string>(Int32.Parse(datareader["id"].ToString()), datareader["altname"].ToString()));
                    }

                    datareader.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return readerList;
        }

        #endregion

        #region GetAllReaderDetails

        /*
         * Functionality    :   Get the list of Reader Details 
         * Parameter        :   Void
         * Return Type      :   List => list of Reader Object
         */
        public List<NWReader> GetAllReaderDetails()
        {
            //Local Variable Declaration
            List<NWReader> readerList = new List<NWReader>();
            List<Antenna> antList = new List<Antenna>();

            string query = string.Empty;
            MySqlCommand command = null;
            MySqlDataReader antdatareader = null;

            DataTable readertable = new DataTable();
            DataTable antennatable = new DataTable();
            //Main Logic
            try
            {
                databaseConnection = GetDBConnection();
                if (databaseConnection == null)
                {
                    throw new Exception("DatabaseConnection is null ");
                }
                else
                {
                    if (databaseConnection.State != ConnectionState.Open)
                    {
                        databaseConnection.Open();
                    }
                    CmdString = "SELECT `id`, `model`, `serialno`, `altname`, `ipaddress`, `beep` FROM `reader` WHERE 1";
                    //CmdString = "SELECT r.id readerid,model,serialno,r.altname readeraltname,ipaddress,beep,a.id antid,name,a.altname antaltname,power FROM reader r RIGHT JOIN antenna a ON r.id = a.readerid";
                    cmd = new MySqlCommand(CmdString, databaseConnection);
                    datareader = cmd.ExecuteReader();
                    readertable.Load(datareader);
                    datareader.Close();
                    foreach (DataRow row in readertable.Rows)
                    {
                        query = "SELECT `id`, `name`, `altname`, `power` FROM `antenna` WHERE readerid='" + row["id"].ToString() + "'";
                        command = new MySqlCommand(query, databaseConnection);
                        antdatareader = command.ExecuteReader();
                        antennatable.Clear();
                        antennatable.Load(antdatareader);
                        antdatareader.Close();
                        //antList.Clear();
                        antList = new List<Antenna>();
                        foreach (DataRow antrow in antennatable.Rows)
                        {
                            antList.Add(new Antenna(Int32.Parse(antrow["id"].ToString()), antrow["altname"].ToString(), antrow["name"].ToString(), Int32.Parse(antrow["power"].ToString())));
                        }
                        readerList.Add(new NWReader(Int32.Parse(row["id"].ToString()), row["model"].ToString(), row["altname"].ToString(), row["ipaddress"].ToString(), (row["beep"].ToString() == "0") ? false : true, antList));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
            return readerList;
        }

        #endregion
    }
}
