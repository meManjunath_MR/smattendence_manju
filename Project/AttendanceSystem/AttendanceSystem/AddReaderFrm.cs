﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace AttendanceSystem
{
    public partial class AddReaderFrm : Form
    {
        #region Variable Declaration
        //Variable Declaration

        RFIDReaderClass readerObj = new RFIDReaderClass();
        RFIDReaderDB readerDBObj = new RFIDReaderDB();
        string serialno = string.Empty;
        string model = string.Empty;
        int readerid = 0;

        #endregion

        public AddReaderFrm()
        {
            InitializeComponent();
        }

        #region FormLoad
        private void AddReaderFrm_Load(object sender, EventArgs e)
        {
            ClearData();
        }
        #endregion

        #region CheckButton

        private void CheckBtn_Click(object sender, EventArgs e)
        {
            //Local Variable Declartion
            Boolean readerConnected = false;
            List<KeyValuePair<string, string>> readerInfo = new List<KeyValuePair<string, string>>();

            //Main Logic
            try
            {
                if (IpTxt.Text == string.Empty)
                {
                    MessageBox.Show(this, "Please Enter Valid IP Address", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ClearData();
                }
                else
                {
                    Match match = Regex.Match(IpTxt.Text.Trim(), @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
                    if (match.Success)
                    {
                        readerConnected = readerObj.deviceConnected(IpTxt.Text.Trim());
                        if (readerConnected) //Show the Status of the Reader
                        {
                            //if connected , get the reader information , such as hostname
                            ReaderStatusLbl.Text = "Reader Status : Connected ";
                            readerInfo = readerObj.GetReaderInfo(IpTxt.Text.Trim());
                            foreach(KeyValuePair<string,string> info in readerInfo)
                            {
                                switch(info.Key)
                                {
                                    case "model":   ReaderStatusLbl.Text += " Model : " + info.Value;
                                                    model = info.Value;
                                        break;
                                    case "serial":  serialno = info.Value;
                                        break;
                                }
                            }
                            ReaderPanel.Enabled = true;
                        }
                        else
                        {
                            ReaderStatusLbl.Text = "Reader Status : Not Connected ";
                            ReaderPanel.Enabled = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "Please Enter Valid IP Address", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        ClearData();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(this,"Error in Check Button : "+ex.Message,"Information",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        #endregion

        #region SaveButton
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            //Local Variable Declaration
            Boolean readerConnected = false;
            string query = string.Empty;

            //Main Logic
            //First check whether reader is connected or not again .

            try
            {
                if (IpTxt.Text == string.Empty)
                {
                    MessageBox.Show(this, "Please Enter Valid IP Address", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    IpTxt.Focus();
                }
                else if(AltNameTxt.Text == string.Empty)
                {
                    MessageBox.Show(this, "Please Enter Valid Alternate Name", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    AltNameTxt.Focus();
                }
                //check whether name field doesnot exceed 25 character
                else if(AltNameTxt.Text.Length > 25)
                {
                    MessageBox.Show(this, "Alternate Name cannot exceed 25 Character", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    AltNameTxt.Focus();
                }
                else
                {
                    Match match = Regex.Match(IpTxt.Text.Trim(), @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
                    if (match.Success)
                    {
                        readerConnected = readerObj.deviceConnected(IpTxt.Text.Trim());
                        if (readerConnected) //Show the Status of the Reader
                        {
                            if (model != string.Empty && serialno != string.Empty)
                            {
                                //if connected , save the data to database
                                //check whether same ipaddress is inserted again or not
                                if (!readerDBObj.IsReaderExist(IpTxt.Text.Trim(), AltNameTxt.Text))
                                {

                                    query = "INSERT INTO `reader`(`model`, `serialno`, `altname`, `ipaddress`, `beep`) " +
                                            "VALUES ('" + model + "','" + serialno + "','" + AltNameTxt.Text + "','" + IpTxt.Text + "','0')";
                                    readerid = readerDBObj.InsertData(query);

                                    MessageBox.Show(this, "Reader Details inserted Successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    //enable the antenna panel
                                    AntennaGroup.Enabled = true;

                                    //Add the Antenna label and Antenna Text
                                }
                                else
                                {
                                    MessageBox.Show(this, "IPAddress or Reader Alternate Name Already Exist", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    ClearData();
                                }

                            }
                            else
                            {
                                MessageBox.Show(this, "Please Check Whether Reader Connected Or Not", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                ClearData();
                            }
                            
                        }
                        else
                        {
                            ReaderStatusLbl.Text = "Reader Status : Not Connected ";
                            ReaderPanel.Enabled = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "Please Enter Valid IP Address", "Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        ClearData();
                    }
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(this, "Error in Save Button : " + ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region ClearData

        /*
         * Functionality    :   Reset the Variable
         * Parameter        :   void
         * Return Type      :   void
        */
        private void ClearData()
        {
            IpTxt.Text = string.Empty;
            ReaderStatusLbl.Text = "Reader Status";
            ReaderPanel.Enabled = false;
            AntennaGroup.Enabled = false;
            serialno = string.Empty;
            model = string.Empty;
            readerid = 0;
            IpTxt.Focus();
        }

        #endregion
    }
}
