-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2017 at 01:14 PM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rfidreader`
--

-- --------------------------------------------------------

--
-- Table structure for table `antenna`
--

CREATE TABLE `antenna` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `altname` varchar(25) NOT NULL,
  `readerid` int(11) NOT NULL,
  `power` int(11) NOT NULL,
  `transactiontime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `antenna`
--

INSERT INTO `antenna` (`id`, `name`, `altname`, `readerid`, `power`, `transactiontime`) VALUES
(1, 'ant1', 'entry', 1, 30, '2017-09-14 13:31:40'),
(2, 'ant2', 'exit', 1, 26, '2017-09-14 13:31:40'),
(3, 'ant1', 'entry', 2, 30, '2017-09-14 13:32:43'),
(4, 'ant1', 'entry', 3, 30, '2017-09-14 13:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `empcode` varchar(10) NOT NULL,
  `designation` varchar(25) NOT NULL,
  `phoneno` varchar(15) NOT NULL,
  `altphoneno` varchar(15) NOT NULL,
  `bloodgrp` varchar(5) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `transactiontime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reader`
--

CREATE TABLE `reader` (
  `id` int(11) NOT NULL,
  `model` varchar(20) NOT NULL,
  `serialno` varchar(20) NOT NULL,
  `altname` varchar(25) NOT NULL,
  `ipaddress` varchar(15) NOT NULL,
  `beep` varchar(1) NOT NULL,
  `transactiontime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reader`
--

INSERT INTO `reader` (`id`, `model`, `serialno`, `altname`, `ipaddress`, `beep`, `transactiontime`) VALUES
(1, 'Astra-EX', '213322', 'reception_reader', '192.168.0.190', '0', '2017-09-14 08:02:09'),
(2, 'Astra-EX', '213321', 'reader2', '192.168.0.191', '0', '2017-09-14 08:06:37'),
(3, 'Astra-EX', '213320', 'reader3', '192.168.0.192', '0', '2017-09-14 08:10:22');

-- --------------------------------------------------------

--
-- Table structure for table `userdetails`
--

CREATE TABLE `userdetails` (
  `id` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(20) NOT NULL,
  `emailid` varchar(50) NOT NULL,
  `phoneno` varchar(15) NOT NULL,
  `transactiontime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userdetails`
--

INSERT INTO `userdetails` (`id`, `username`, `password`, `emailid`, `phoneno`, `transactiontime`) VALUES
(1, 'admin', 'admin', 'nisha@keenkite.com', '9632632581', '2017-09-20 07:50:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `antenna`
--
ALTER TABLE `antenna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reader`
--
ALTER TABLE `reader`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userdetails`
--
ALTER TABLE `userdetails`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `antenna`
--
ALTER TABLE `antenna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reader`
--
ALTER TABLE `reader`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `userdetails`
--
ALTER TABLE `userdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
